
from datetime import date
from fastapi_users import schemas


class UserRead(schemas.BaseUser[int]):
    user_name: str
    salary: float
    promotion_date: date


class UserCreate(schemas.BaseUserCreate):
    user_name: str
    salary: float
    promotion_date: date
    


