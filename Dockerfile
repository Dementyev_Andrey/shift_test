FROM python:3.9.16

SHELL ["/bin/bash", "-c"]

WORKDIR .

COPY . .

RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install

RUN chmod +x /entrypoint.sh