# shift_test

## Примечание по работе со swagger:
1. После запуска приложения (любым из указанных способов) swagger будет доступен по http://127.0.0.1:8000/docs
2. Для авторизации в поле username требуется ввести email(указанный при регистрации), не успел разобраться

## Запуск проекта
1. При помощи docker-compose(рекомендуемо):
    - Переименуйте файл **mv .env.example_for_docker .env**
    - docker-compose up --build -d
    
2. Обычная запуск на Linux (ubuntu):
    1. Создайте и активируйте виртуальное окружение с версией python 3.9 удобным для вас способом
        - *Я использую anaconda*:
        - conda create -n <your_env_name> python=3.9
        - conda activate <your_env_name>
    2. Создайте базу данных в Postgresql:
        - psql -U root -d postgres -W
        - create database <your_db_name>
    3. Cоздание пользователя :
        - **CREATE USER <user_name> WITH PASSWORD '<your_assword>';**
        - Даем права на базу командой **GRANT ALL PRIVILEGES ON DATABASE "<your_db_name>" to <user_name>;**
        - Теперь подключаемся к базе, к которой хотим дать доступ **\c <you_db_name>**
        - Добавим все права на использование всех таблиц в базе
         **GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO "<user_name>";**
    3. Настроечный файл:
        - Переименуйте файл **mv .env.example_standart .env**
        - Заполните информацию о базе в соответствии уже существующей (см. п2)
    4. Установка зависимостей(при активированной виртуальной среде):
        - **pip install poetry**
        - **poetry install**
    5. Запуск приложения:
        - применение миграций к базе данных **alembic upgrade head**
        - запуск приложения **uvicorn main:app --host 0.0.0.0 --port 8000**
        
