from fastapi import Depends, FastAPI
from fastapi_users import FastAPIUsers, fastapi_users
from auth.auth import auth_backend
from auth.database import User
from auth.manager import get_user_manager
from auth.schema import UserCreate, UserRead
from fastapi.responses import JSONResponse


fastapi_users = FastAPIUsers[User, int](get_user_manager, [auth_backend],)

current_user = fastapi_users.current_user()

app = FastAPI(title="SHIFT TEST")

app.include_router(
    fastapi_users.get_auth_router(auth_backend), prefix="/auth/jwt", tags=["auth"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate), prefix="/auth", tags=["auth"],
)

@app.get("/get_info")
def get_info(user: User = Depends(current_user)):
    json_compatible_item_data = {"salary": user.salary, "promotion_date": (user.promotion_date).strftime("%Y-%b-%d")}
    return JSONResponse(content=json_compatible_item_data)