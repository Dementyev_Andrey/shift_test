import os
from dotenv import load_dotenv


load_dotenv()

POSTGRES_DB = os.environ.get("POSTGRES_DB", "db")
POSTGRES_USER = os.environ.get("POSTGRES_USER", "db")
POSTGRES_PORT = os.environ.get("POSTGRES_PORT", "db")
POSTGRES_PASSWORD = os.environ.get("POSTGRES_PASSWORD", "db")
POSTGRES_HOST = os.environ.get("POSTGRES_HOST", "db")

SECRET_KEY_TOKEN = os.environ.get("SECRET_KEY", "secret")
SECRET_KEY_FOR_PASSWORD_WORK = os.environ.get("SECRET_KEY_FOR_PASSWORD_WORK", "secret")
